# README guidelines
The purpose of this project is to show what git commands exist and what are they for.

## Overview

Everthing is as in the title.

## Installation

No installation required.

## Basic use

- Describe git command.
- Explain it's function.
- Optional: each of these commands may include link to youtube chanell which explains them in detail and shows how they are working.

## API

No API is needed.

**Options objects**

No Option objects are required

## Examples

git branch <name> : create new branch (3.2 https://www.youtube.com/watch?v=ydtgQSaUzw0&list=PLDyvV36pndZFHXjXuwA_NywNrVQO0aQqb&index=12)

## Tests

No need for tests. 

## Contributors

No extra contributors are required

## License

Link to the license, with a short description of what it is, e.g. "MIT" or
whatever. Ideally, avoid putting the license text directly in the README; link
to it instead.